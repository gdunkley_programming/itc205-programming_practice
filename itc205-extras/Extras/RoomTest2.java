package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 *
 * @author graham
 */
@ExtendWith(MockitoExtension.class)
public class RoomTest2 {
       
    @Mock Booking booking;
    @Spy List<Booking> bookings = new ArrayList<>();
    
    int roomid = 0;
    RoomType roomtype = RoomType.SINGLE;
    
    @InjectMocks Room room = new Room(roomid, roomtype);
    
    
    
    @BeforeEach
    public void setUp() {
//        bookings.add(booking);
//        room = new Room(roomid, roomtype);

        
    }
    
    
   @Test
    public void testCheckoutIfInOccupiedState(){
        //arrange
        bookings.add(booking);
        room.checkin();
        assertEquals(1, bookings.size());
        assertTrue(room.isOccupied());
        
        //act 
        room.checkout(booking);
        
        //assert
        verify(bookings).remove(booking);
        assertTrue(room.isReady());
        assertEquals(0, bookings.size());
    }
}
