/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author graham
 */
import hotel.checkout.CheckoutCTL;
import hotel.checkout.CheckoutUI;
import hotel.checkout.CheckoutUI.State;
import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardHelper;
import hotel.credit.CreditCardType;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.String;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.function.Executable;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class) 
public class CheckoutCTLIntergration {
    
    //
    @Mock CheckoutUI mkCheckoutUI;
        // might need to add others for checkoutUI
    @Mock Hotel mkhotel;
        //
    @Mock Booking mkbooking;
        // add booking mocks
        @Mock Guest guest;
	@Mock Room room;
	@Mock Date bookedArrival; 
	int stayLength;
	int numberOfOccupants;
	long confirmationNumber;
	CreditCard creditCard;
	List<ServiceCharge> charges;
	State state;
        
    
    @Mock ServiceCharge mkServiceCharge;
        @Mock ServiceType mkServiceType;
	double cost;
        
    @Mock Room mkRoom;
        
    
    @Mock CreditAuthorizer mkCreditAuth;
    
            
          
            
            
    
    
    
    
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testCreditDetailsThrowsRuntimeException() {
        //arrange
        
        
        //act
        
        
        //assert
        
        
    }
    
    
    
    
    
    
}