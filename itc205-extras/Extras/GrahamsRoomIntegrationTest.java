/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;

/**
 *
 * @author graham
 */
public class RoomIntegrationTest {
    
    public enum State {READY, OCCUPIED}
    
    @Mock Room mkInstance;
    @Mock RoomType mkRType;
    @Mock State mkstate;
    @Mock Booking mkBooking;
    @Mock Guest mkguest;
    @Mock Date mkArrivalDate;
    @Mock CreditCard mkCreditCard;
    @Mock CreditCardType mkCCType;
    
    int idRoom, numberCCard, ccv, stayLength, numOccupants;
    List<Booking> bookings = new ArrayList<>();
    SimpleDateFormat dateFormat;
    
    
    public RoomIntegrationTest() {
        
        
    }
    
    
    @Before
    public void setUp() throws ParseException {
        mkRType = RoomType.SINGLE;
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        mkArrivalDate = dateFormat.parse("22-12-2012");
        mkCCType = CreditCardType.VISA;
        numberCCard = 1111;
        ccv = 1;
        mkCreditCard = new CreditCard(mkCCType, numberCCard, ccv);
        stayLength = 1;
        numOccupants = 1;
        idRoom = 101;
        
    }
    
 
    /**
     * Test of book method, of class Room.
     */
    @Test
    public void testBook() {
        System.out.println("book");
        
//        Room instance = null;
        
        
        Booking expResult = null;
        Booking result = mkInstance.book(mkguest, mkArrivalDate, stayLength, numOccupants, mkCreditCard);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
//
//    /**
//     * Test of checkin method, of class Room.
//     */
//    @Test
//    public void testCheckin() {
//        System.out.println("checkin");
//        Room instance = null;
//        instance.checkin();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of checkout method, of class Room.
//     */
//    @Test
//    public void testCheckout() {
//        System.out.println("checkout");
//        Booking booking = null;
//        Room instance = null;
//        instance.checkout(booking);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
