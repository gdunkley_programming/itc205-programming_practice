package hotel.entities;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;


    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    /*
        public void creditDetailsEntered(CreditCardType type, int number, int ccv) {
        throws a RuntimeException if state is not CREDIT
        creates a new CreditCard
        calls CreditAuthorizer.authorize()
        if approved
            calls hotel.checkout()
            calls UI.displayMessage() with Credit card debited message
            sets state to COMPLETED
            sets UI state to COMPLETED
        else
            calls UI.displayMessage() with Credit not approved message

    */



@RunWith(MockitoJUnitRunner.class)
public class TestHotelGrahams {
	
	@Spy Map<Integer, Guest> guests = new HashMap<>();
	@Mock Guest guest;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@InjectMocks Hotel hotel = new Hotel();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	
	@Test
	public void testIsRegistered() {
		
		boolean actual = hotel.isRegistered(1);
		
		assertFalse(actual);
		verify(guests).containsKey(anyInt());
	}

	
	@Test(expected=RuntimeException.class)
	public void testRegisterGuestAlreadyRegistered1() {
		//arrange
		guests.put(1, guest);
		
		//act
		hotel.registerGuest("A", "Z", 1);
		
		//assert
	}
	
	
	@Test
	public void testRegisterGuestAlreadyRegistered2() {
		//arrange
		guests.put(1, guest);
		exception.expect(RuntimeException.class);
		exception.expectMessage("Phone number already registered");
		
		//act
		hotel.registerGuest("A", "Z", 1);
		
		//assert
	}
}