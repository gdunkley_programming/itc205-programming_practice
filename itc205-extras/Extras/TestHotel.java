package hotel.entities;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;



@RunWith(MockitoJUnitRunner.class)
public class TestHotel {
	
	@Spy Map<Integer, Guest> guests = new HashMap<>();
	@Mock Guest guest;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@InjectMocks Hotel hotel = new Hotel();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	
	@Test
	public void testIsRegistered() {
		
		boolean actual = hotel.isRegistered(1);
		
		assertFalse(actual);
		verify(guests).containsKey(anyInt());
	}

	
	@Test(expected=RuntimeException.class)
	public void testRegisterGuestAlreadyRegistered1() {
		//arrange
		guests.put(1, guest);
		
		//act
		hotel.registerGuest("A", "Z", 1);
		
		//assert
	}
	
	
	@Test
	public void testRegisterGuestAlreadyRegistered2() {
		//arrange
		guests.put(1, guest);
		exception.expect(RuntimeException.class);
		exception.expectMessage("Phone number already registered");
		
		//act
		hotel.registerGuest("A", "Z", 1);
		
		//assert
	}
}
