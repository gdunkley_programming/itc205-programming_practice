/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;

import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

import java.util.ArrayList;
import java.util.Date;


@ExtendWith(MockitoExtension.class)
public class junit_skellton {
    

	
	//Mocks have the exact appearance of the real thing - to anything using them they ARE the real thing
	//you use them to substitute for some class your SUT has a dependency on
	//However, they are just a shell or a mask - you have to tell them what to do when their methods get called
	//leading to - 'when - then - verify' protocol
	//when a method gets called (with specified parameters)
	//then return some value - you tell it what that is - the mock skips to returning that result with no implementation
	//this is called 'setting the expectiations'
	//once you set the expectations (part of the 'arrange' phase)
	//you 'act' - ie call the method to be tested
	//then you 'verify'  - i.e. check that the mocked method actually got called with the parameters you expected
	@Mock Booking booking;
	
	//spies are a real thing with an instrumentation layer - they have 'pass through' semantics
	//you can query them to see if certain methods are called with certain values
	//but once they log that, real things happen 
	//when you add an element to a spy ArrayList, it logs it, AND the element gets added to a real ArrayList
	//Note that you DONT call the constructor (unless there is no default no-args constructor)
	@Spy ArrayList<Booking> bookings;

	//InjectMocks is where 'magically' fields initialised inside the SUT
	//constructor are substituted by mocks and spys declared in the test class
	//this is extremely useful since it allows those fields to be manipulated
	//and inspected outside the class, even if they are private
	
	//InjectMocks happens before EVERY test - so it doesn't have to go inside the @BeforeEach method
	
	@InjectMocks Room room = new Room(1, RoomType.SINGLE);
	//Need to specify the constructor here because Room doesnt have a default no-args constructor
	

	@BeforeEach
	void setUp() throws Exception {
		
	}

	
	@AfterEach
	void tearDown() throws Exception {
	}

	
	@Test
	void testForBooleanOutcome() {
		//arrange
		//act
		//assert
		assertTrue(true); //substitute boolean expression for true		
	}

	
	@Test
	void testForSomeExceptionGettingThrown() {
		//arrange
		//act
		Executable e = () -> room.checkin();
		Throwable t = assertThrows(RuntimeException.class, e);
		//assert
		assertEquals("Cannot checkin to a room that is not READY", t.getMessage());
	}
	
	
	
	@Test
	void testUsingASpy() {
		//arrange
		bookings.add(booking); //bookings is a spy that hides bookings in the SUT
		assertEquals(1, bookings.size());
		//act
		room.checkout(booking);
		
		//assert
		verify(bookings).remove(booking); //spies let you check whether certain things happened
		assertEquals(0, bookings.size()); // they pass the call through to a real implementation
	}

	
	@Test
	void testMakingAMockDoSomething() {
		//when - then - verify protocol
		//arrange
		bookings.add(booking);
		//booking is a mock - so we need to tell it how to respond when one of its methods gets called
		when(booking.doTimesConflict(any(Date.class), anyInt())).thenReturn(false);
		//any() and anyInt are Matchers - they tell this line that any Date and an int can be expected
		//note the when- then protocol for specifying expectations;
		
		//act
		Booking actual = room.book(guest, arrivalDate, stayLength, numberOfOccupants, creditCard);

		//assert
		verify(booking).doTimesConflict(arrivalDate, stayLength);
		//verify checks whether a mocks methods were called with the specified parameters - can use matchers here too
		assertNotNull(actual);
		assertTrue(bookings.contains(actual));
	}
	

}
