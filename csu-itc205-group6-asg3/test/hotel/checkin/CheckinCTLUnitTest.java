/**
 * Unit test for CheckinCTL method checkInConfirmed.
 * @author: Bernard Prosser
 * @version: 1.0 - 15/09/18
 */

package hotel.checkin;

import hotel.credit.CreditCard;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckinCTLUnitTest {

    //default values
    private static final long CONFIRMATION_NUMBER = 999;

    @Mock Booking booking;
    @Mock Guest guest;
    @Mock CreditCard creditCard;
    @Mock Room room;
    @Mock CheckinUI checkinUI;
    @Mock Hotel hotel;
    long confirmationNumber;

    @InjectMocks
    CheckinCTL ctl = new CheckinCTL(hotel);

    @BeforeEach
    public void setUp() {
        confirmationNumber = CONFIRMATION_NUMBER;
    }


    /**
     * This method prepares the behaviour of the mocks so that the controller's
     * state can be changed via the confirmationNumberEntered method. This had
     * to be pulled out of the @BeforeEach setUp method, otherwise Mockito
     * complained about unnecessary stubs. It's now called by each test that
     * requires the controller to be in the CONFIRMING state.
     */
    private void prepareMocksToChangeCtlStatus() {
        when(hotel.findBookingByConfirmationNumber(any(Long.class))).thenReturn(booking);
        when(booking.isPending()).thenReturn(true);
        when(booking.getRoom()).thenReturn(room);
        when(room.isReady()).thenReturn(true);

        when(booking.getGuest()).thenReturn(guest);
        when(booking.getCreditCard()).thenReturn(creditCard);

        when(room.getDescription()).thenReturn("Description");
        when(room.getId()).thenReturn(1);
        when(booking.getArrivalDate()).thenReturn(new GregorianCalendar(2011, 6, 15).getTime());
        when(booking.getStayLength()).thenReturn(5);
        when(guest.getName()).thenReturn("Joe Bloggs");
        when(creditCard.getVendor()).thenReturn("Card Vendor");
        when(creditCard.getNumber()).thenReturn(999);
    }


    @Test
    public void testConfirmCheckInWhenNotConfirmingThrowsException() {
        //arrange
        assertFalse(ctl.isConfirming(), "Controller is in CONFIRMING state");
        Executable e = () -> ctl.checkInConfirmed(true);

        //act
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Can't confirm check-in when not in CONFIRMING state", t.getMessage());
    }


    @Test
    public void testBehaviourWhenCheckInIsConfirmed() {
        //arrange
        prepareMocksToChangeCtlStatus();
        ctl.confirmationNumberEntered(confirmationNumber);
        assertTrue(ctl.isConfirming(), "Controller is not in CONFIRMING state");

        //act
        ctl.checkInConfirmed(true);

        /*
            Not checking state of the UI as this will be checked
            during integration testing
         */

        //assert
        verify(hotel).checkin(confirmationNumber);
        verify(checkinUI).displayMessage("Check-in confirmed");
        assertTrue(ctl.isCompleted(), "Controller is not in COMPLETED state");
    }


    @Test
    public void testBehaviourWhenCheckInIsNotConfirmed() {
        //arrange
        prepareMocksToChangeCtlStatus();
        ctl.confirmationNumberEntered(confirmationNumber);
        assertTrue(ctl.isConfirming(), "Controller is not in CONFIRMING state");

        //act
        ctl.checkInConfirmed(false);

        /*
            Not checking state of the UI as this will be checked
            during integration testing
         */

        //assert
        verify(checkinUI).displayMessage("Check-in cancelled");
        assertTrue(ctl.isCancelled(), "Controller is not in CANCELLED state");
    }
}