/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.checkout;


import hotel.checkout.CheckoutCTL;
import hotel.checkout.CheckoutUI;
import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardHelper;
import hotel.credit.CreditCardType;
import hotel.entities.Booking;
import hotel.entities.Hotel;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.String;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.function.Executable;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class) 
public class CheckoutCTLTest {
       
    @Mock CreditCard mockingCreditCard;
    @Mock CheckoutUI mockingCheckoutUI;
    @Mock Booking mockingBooking;
    @Mock CreditAuthorizer creditAuthoriser;
    @Mock CreditCardHelper creditCardHelper;
    @Mock Hotel mockHotel;
    
    int number = 1;
    int roomId = 101;
    int ccv = 0;
    double total = 101.00;
       
    @InjectMocks CheckoutCTL testCheckoutCTL = new CheckoutCTL(mockHotel);    
        
    @Before
    public void setUp() {
        testCheckoutCTL.state = CheckoutCTL.State.CREDIT;
        testCheckoutCTL.roomId = roomId;
        testCheckoutCTL.total = total;
        
    }
    
    @After
    public void tearDown() {
    }
    
  
    
    @Test
    public void testCreditDetailsThrowsRuntimeException() {
        //arrange
        testCheckoutCTL.isCancelled();
        
        //act
        Executable e = () -> testCheckoutCTL.creditDetailsEntered(CreditCardType.VISA, number, ccv);
        Throwable throwRTE = assertThrows(RuntimeException.class, e);
        
        //assert
        assertEquals("Error - CheckoutCTL is in bad state: != CREDIT", throwRTE.getMessage());
        
    }
    
    
    public void testcreditDetailsEnteredStateCreditApproved() {
        //arrange
        
        String msg1 = String.format(
        "credit card " + number +
                " was debited the ammout of :" + total);
        String msg2 = String.format("Declined - credit card number " +
                        number + " has been declined for the amount of " +
                        total + "\n Please contact your financal institution");

        when(creditCardHelper.makeCreditCard(CreditCardType.VISA, number, ccv)).thenReturn(mockingCreditCard);
        when(creditAuthoriser.authorize(mockingCreditCard, total)).thenReturn(Boolean.TRUE);
        
        //act
        testCheckoutCTL.creditDetailsEntered(CreditCardType.VISA, number, ccv);       
        
        //assert
        verify(creditCardHelper).makeCreditCard(CreditCardType.VISA, number, ccv);
        verify(creditAuthoriser).authorize(mockingCreditCard, total);
        verify(mockHotel).checkout(roomId);
        verify(mockingCheckoutUI).displayMessage(msg1);
        verify(mockingCheckoutUI).setState(CheckoutUI.State.COMPLETED);
        assertTrue(testCheckoutCTL.state == CheckoutCTL.State.COMPLETED);
        
        
    }
    
}
