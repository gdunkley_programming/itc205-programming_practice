/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.checkout;

import hotel.checkout.CheckoutCTL;
import hotel.checkout.CheckoutUI;
import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardHelper;
import hotel.credit.CreditCardType;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class) 
public class CheckoutCTLIntegrationTest {
    
    @Mock CheckoutUI mkCheckoutUI;
    @Mock Booking mkBooking;
    @Mock CreditAuthorizer mkCCAuth;
    @Mock CreditCardHelper mkCCHelper;
    
    public @Rule ExpectedException exception = ExpectedException.none();
    
    CheckoutCTL control;
    Hotel hotel = new Hotel();
    
    SimpleDateFormat format;
    Date arrivalDate;
    CreditCard creditCard;
    CreditCardType ccType = CreditCardType.VISA;

    int ccNumber = 1;
    int ccv = 0;
    double total = 101.00;
    int roomId = 101;
    String msg1;
    String msg2;
    String msg3;
    String msg4;
    
    int phone = 987654;
    int stay = 1;
    long confNum;
    int occupantNum = 1;
    
    @Before
    public void setUp() throws Exception {
        creditCard = new CreditCard(ccType, ccNumber, ccv);
               
        msg1 = String.format(
        "credit card " + ccNumber +
                " was debited the ammout of :" + total);
        msg2 = String.format("Declined - credit card number " +
                        ccNumber + " has been declined for the amount of " +
                        total + "\n Please contact your financal institution");
        msg3 = "Declined - credit card has been declined Please contact your financal institution";
        msg4 = String.format("Declined - credit card has been declined Please contact your financal institution");
        /*
        Hotel implementation to have room to be 'checkout'ed need:
        Hotel obj
        registerGuest(String name, String address, int phoneNumber)        
        addRoom(RoomType roomType, int id)
        registerGuest(String name, String address, int phoneNumber) 
        findGuestByPhoneNumber(int phoneNumber)
        Set arrival Date with format
        Book Hotel get Confirmation
        checkin(long confirmationNumber)
        Create real CheckoutCTL for hotel object.
        
        */
        
        //real Hotel implementation and CheckoutCTL
        hotel.registerGuest("John", "happy St", phone);
        Room room = new Room(roomId, RoomType.SINGLE);      //created room but variable order is different to the hotel.addRoom
        hotel.addRoom(RoomType.SINGLE, roomId);             // dont want to change as it may alter other groups code and break it.
        Guest guest = hotel.findGuestByPhoneNumber(phone);
        format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("01-01-2001");
        confNum = hotel.book(room, guest, arrivalDate, stay, occupantNum, creditCard);
        hotel.checkin(confNum);
        control = new CheckoutCTL(hotel);
        
        //Mocking remaining mocks
        MockitoAnnotations.initMocks(this);
        control.checkoutUI = mkCheckoutUI;
        control.creditAuthoriser = mkCCAuth;
        control.creditCardHelper = mkCCHelper;
        
        //setting state for CheckoutCTL
        control.state = CheckoutCTL.State.ROOM;
        control.roomIdEntered(roomId);
        control.chargesAccepted(true);

    }

    @Test
    public void testCreditDetailsEnteredCreditAuthorisedTRUE() {
        //arrange
        //setUp performed inital arragements
        
        assertTrue(control.state == CheckoutCTL.State.CREDIT);      // check CheckoutCTL state is in the CREDIT state to start.
        Booking realBooking = hotel.findActiveBookingByRoomId(roomId);      //create a realBooking object to reference.
        assertTrue(realBooking.isCheckedIn());      // check we have booked in a room with real objects
        Room realRoom = realBooking.getRoom();      // create room object
        assertFalse(realRoom.isReady());
        
        
        when(mkCCHelper.makeCreditCard(any(), anyInt(), anyInt())).thenReturn(creditCard);      //make CChelper return real Credit card
        when(mkCCAuth.authorize(any(), anyDouble())).thenReturn(true);      //make sure the Credit card is approved.
        
        //act
        control.creditDetailsEntered(ccType, ccNumber, ccv);                
        
        
        //assert
        assertFalse(control.state == CheckoutCTL.State.CREDIT);         //check CheckoutCTL state isn't still in CREDIT
        assertTrue(control.state == CheckoutCTL.State.COMPLETED);       //check CheckoutCTL state is correct in COMPLETED
        verify(mkCheckoutUI).setState(CheckoutUI.State.COMPLETED);      //check UI is in completed state
        verify(mkCheckoutUI, times(3)).displayMessage(anyString());     //check message has been displayed.
        
        assertTrue(realRoom.isReady());     //ensure room has been released and in ready state
        assertTrue(realBooking.isCheckedOut());     //ensure booking has been checkout and ready for next booking

               
    }
    

    @Test
    public void testCreditDetailsEnteredCreditAuthorisedFALSE() {
        //arrange
        //setUp performed inital arragements
        
        assertTrue(control.state == CheckoutCTL.State.CREDIT);      // check CheckoutCTL state is in the CREDIT state to start.
        Booking realBooking = hotel.findActiveBookingByRoomId(roomId);      //create a realBooking object to reference.
        assertTrue(realBooking.isCheckedIn());      // check we have booked in a room with real objects
        Room realRoom = realBooking.getRoom();      // create room object
        assertFalse(realRoom.isReady());
        
        
        when(mkCCHelper.makeCreditCard(any(), anyInt(), anyInt())).thenReturn(creditCard);      //make CChelper return real Credit card
        when(mkCCAuth.authorize(any(), anyDouble())).thenReturn(false);      //make sure the Credit card is NOT approved.
        
        //act
        control.creditDetailsEntered(ccType, ccNumber, ccv);                
        
        
        //assert
        assertTrue(control.state == CheckoutCTL.State.CREDIT);         //check CheckoutCTL state is still in CREDIT
        assertFalse(control.state == CheckoutCTL.State.COMPLETED);       //check CheckoutCTL state hasn't moved to COMPLETED state
        verify(mkCheckoutUI, times(3)).displayMessage(anyString());     //check message has been displayed.
        assertFalse(realRoom.isReady());     //ensure room has not been released..
        assertFalse(realBooking.isCheckedOut());     //ensure booking isn't checkout
        assertNotNull(hotel.findActiveBookingByRoomId(roomId));        //confirm booking still exists.
    }
    
    
    @Test
    public void testCreditDetailsEnteredCreditStateNotCREDIT() {
//        //arrange
//        //setUp performed inital arragements
        
        assertTrue(control.state == CheckoutCTL.State.CREDIT);      // check CheckoutCTL state is in the correct state to start test.
        Booking realBooking = hotel.findActiveBookingByRoomId(roomId);      //create a realBooking object to reference.
        assertTrue(realBooking.isCheckedIn());      // check we have booked in a room with real objects
        Room realRoom = realBooking.getRoom();      // create room object
        assertFalse(realRoom.isReady());
        
        exception.expect(RuntimeException.class);       //add in junit4 exception RTE catch.
        control.state = CheckoutCTL.State.CANCELLED;    //change CheckoutCTL state is CANCELED so it fails;
        
        //act
        control.creditDetailsEntered(ccType, ccNumber, ccv);                
        
        
        //assert
        assertFalse(control.state == CheckoutCTL.State.CREDIT);         //check CheckoutCTL state is still in CREDIT

        verify(mkCheckoutUI, times(3)).displayMessage(anyString());     //check message has been displayed.
        assertFalse(realRoom.isReady());     //ensure room has not been released..
        assertFalse(realBooking.isCheckedOut());     //ensure booking isn't checkout
        assertNotNull(hotel.findActiveBookingByRoomId(roomId));        //confirm booking still exists.
    }
    
    
}