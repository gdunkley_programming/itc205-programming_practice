/**
 * Unit test for Booking methods checkIn, checkOut and addServiceCharge.
 * @author: Bernard Prosser
 * @version: 1.0 - 15/09/18
 */

package hotel.entities;

import hotel.credit.CreditCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class BookingUnitTest {

    // default values
    private static final int STAY_LENGTH = 5;
    private static final int NUMBER_OF_OCCUPANTS = 1;

    @Mock Guest guest;
    Room room = new Room(1, RoomType.SINGLE);
    Date arrivalDate = new GregorianCalendar(2015, 06, 15).getTime();
    int stayLength = STAY_LENGTH;
    int numberOfOccupants = NUMBER_OF_OCCUPANTS;
    @Mock CreditCard creditCard;
    @Spy List<ServiceCharge> charges = new ArrayList<>();
    @InjectMocks
    Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);


    @BeforeEach
    public void setUp() {
        //re-initialise it here in case a test has changed it
        room = new Room(1, RoomType.SINGLE);
        stayLength = STAY_LENGTH;
        numberOfOccupants = NUMBER_OF_OCCUPANTS;
    }


    @Test
    public void testStateAfterCheckIn() {
        //arrange
        assertTrue(booking.isPending(), "Booking is not in pending state");

        //act
        booking.checkIn();

        /*
            Not checking state of Room after check-in as this will be saved until
            integration testing.
         */

        //assert
        assertTrue(booking.isCheckedIn(), "Booking is not in checked-in state after check-in");
    }


    @Test
    public void testCheckInWhenNotPendingThrowsException() {
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn(), "Booking is not in checked-in state");
        Executable e = () -> booking.checkIn();

        //act
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals(t.getMessage(), "Trying to check-in when not in PENDING state");
    }


    @Test
    public void testStateAfterCheckOut() {
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn(), "Booking is not in checked-in state");

        //act
        booking.checkOut();

        /*
            Not checking state of Room after check-out as this will be saved until
            integration testing.
         */

        //assert
        assertTrue(booking.isCheckedOut(), "Booking is not in checked-out state after check-out");
    }


    @Test
    public void testCheckOutWhenNotCheckedInThrowException() {
        //arrange
        assertFalse(booking.isCheckedIn(), "Booking is in checked-in state");
        Executable e = () -> booking.checkOut();

        //act
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals(t.getMessage(), "Trying to check-out when not in CHECKED_IN state");
    }


    @Test
    public void checkServiceChargeAddedToList() {
        //arrange
        int lengthOfChargesAtStartOfTest = charges.size();

        //act
        booking.addServiceCharge(ServiceType.ROOM_SERVICE, 100.0);

        /*
            Not testing the ServiceCharge object that is returned here, as that is
            too dependent on the ServiceCharge class. Purely testing for presence
            of a ServiceCharge in the list, and assuming it is correct.
         */

        //assert
        verify(charges).add(any(ServiceCharge.class));
        assertEquals(lengthOfChargesAtStartOfTest + 1, charges.size(), "Service charge hasn't been added to list");
    }

}