package hotel.entities;

import hotel.credit.CreditCard;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.function.Executable;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class) 
public class RoomTest {
    
    @Mock Booking mockingBooking;
    @Mock Guest mockingGuest;
    @Mock CreditCard mockingCreditCard;
    @Spy List<Booking> spyBookings = new ArrayList<>();
      
    int occupants = 1;
    int roomid =1;
    int stayLength =1;
    
    Date arrivalDate;
    SimpleDateFormat dateFormat;
    
    RoomType roomtype = RoomType.SINGLE;
    
    
    
    
    @InjectMocks Room room = new Room(roomid, roomtype);
//    @InjectMocks Booking booking;
//    @InjectMocks Booking mockingBooking = new Booking();
    
    
    @Before
    public void setUp() throws ParseException {
        // tasks needed prior to all methods for setup
        
        spyBookings.add(mockingBooking);
        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = dateFormat.parse("10-10-2010");
    } 
    
    
    @After
    public void tearDown() {
        //tasks to remove prior to exit.
        
    }
    
    @Test
    public void testCheckinIfInReadyState() {
        //arrange
            //bookings setup in setUp() method.
        
        //act
        room.checkin();
                
        //assert
        assertTrue(room.isOccupied());
    }
    
       
    @Test
    public void testCheckinIfInOcupiedState() {
        //arrange
        room.checkin();                     // checks in room
        assertTrue(room.isOccupied());      // Checks that room becomes 'OCCUPIED'
        
        //act
        Executable e = () -> room.checkin();
        Throwable throwRTE = assertThrows(RuntimeException.class, e);
        
        //assert
        assertEquals("Error - The room is not in the correct state... 'READY'", throwRTE.getMessage());
                // assertEquals confirmes RuntimeException is thrown and caught.
        assertTrue(room.isOccupied());      //Confirms state of room is 'OCCUPIED'
    }
    
    @Test
    public void testCheckoutIfInReadyState() {
        //arrange
        room.checkin();
        assertFalse(room.isReady());        // confirms room checkin successfull
        
            //booking setup in setUp() method.
        //act
        room.checkout(mockingBooking);
        mockingBooking.checkOut();
        
        //assert
        assertTrue(room.isReady());     // confirms room was checked out successfully and is now ready.
        
    }
    
    
     @Test
    public void testCheckinIfBookingCheckedIn() {
        //arrange
                    //bookings setup in setUp() method.
        Booking testBooking = room.book(mockingGuest, arrivalDate, stayLength, occupants, mockingCreditCard);

        //act
        testBooking.checkIn();
        
        //assert
        assertTrue(testBooking.isCheckedIn());

    }
    
    
    
    @Test
    public void testCheckoutIfInReadyStateThrowsExceptions() {
        //arrange
            //bookings setup in setUp() method.
        assertTrue(room.isReady());
        
        //act
        Executable e = () -> room.checkout(mockingBooking);
        Throwable throwRTE1 = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Error - The room is not in the correct state... 'OCCUPIED'", throwRTE1.getMessage());
        
    }
    
    @Test
    public void testCheckoutIfInOccupiedState() {
        //arrange
                    //bookings setup in setUp() method.
        room.checkin();
        assertTrue(room.isOccupied());
        assertEquals(1, spyBookings.size());
                    
        //act
        room.checkout(mockingBooking);
        
        //assert
        verify(spyBookings).remove(mockingBooking);
        assertTrue(room.isReady());
        assertEquals(0, spyBookings.size());
        
    }
    
    
    @Test
    public void testBookReturnsBooking() throws ParseException {
        
        //arrange
            //room book setup in setUp() method.
        
        //act
        Booking testBooking = room.book(mockingGuest, arrivalDate, stayLength, occupants, mockingCreditCard);
        
        //assert
        assertNotNull(testBooking);     // Confirms return bookroom
        assertTrue(testBooking.isPending());        // Confirms room.book state is pending.
        
        
        assertTrue(testBooking.doTimesConflict(arrivalDate, stayLength));    // confirms room booking conflicts with set date/length
        assertFalse(room.isAvailable(arrivalDate, stayLength));     // confirms .isAvailable returns False for set date/length
        
    }
    
    
}