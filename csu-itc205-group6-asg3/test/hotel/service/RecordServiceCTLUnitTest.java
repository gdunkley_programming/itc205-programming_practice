/**
 * Unit test for RecordServiceCTL method serviceDetailsEntered.
 * @author: Bernard Prosser
 * @version: 1.0 - 15/09/18
 */

package hotel.service;

import hotel.entities.Booking;
import hotel.entities.Hotel;
import hotel.entities.ServiceType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecordServiceCTLUnitTest {

    // default values
    private static final int ROOM_NUMBER = 1;
    private static final ServiceType SERVICE_TYPE = ServiceType.ROOM_SERVICE;
    private static final double SERVICE_COST = 100.0;

    @Mock Hotel hotel;
    @Mock Booking mockBooking;
    @Mock RecordServiceUI recordServiceUI;
    int roomNumber;
    ServiceType serviceType;
    double serviceCost;

    @InjectMocks
    RecordServiceCTL ctl = new RecordServiceCTL(hotel);


    @BeforeEach
    public void setUp() {
        roomNumber = ROOM_NUMBER;
        serviceType = SERVICE_TYPE;
        serviceCost = SERVICE_COST;
    }


    /**
     * This method prepares the behaviour of the mocks so that the controller's
     * state can be changed via the roomNumberEntered method. This had
     * to be pulled out of the @BeforeEach setUp method, otherwise Mockito
     * complained about unnecessary stubs. It's now called by each test that
     * requires the controller to be in the SERVICE state.
     */
    private void prepareMocksToChangeCtlStatus() {
        when(hotel.findActiveBookingByRoomId(any(Integer.class))).thenReturn(mockBooking);
    }


    @Test
    public void testServiceDetailsEnteredWhenNotInServiceStateThrowsException() {
        //arrange
        assertFalse(ctl.isStateService(), "Controller is in SERVICE state");
        Executable e = () -> ctl.serviceDetailsEntered(serviceType, serviceCost);

        //act
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Must be in state SERVICED", t.getMessage(), "Unexpected exception message");
    }


    @Test
    public void testBehaviourWhenServiceDetailsEntered() {
        //arrange
        prepareMocksToChangeCtlStatus();
        ctl.roomNumberEntered(roomNumber);
        assertTrue(ctl.isStateService(), "Controller is not in SERVICE state");

        //act
        ctl.serviceDetailsEntered(serviceType, serviceCost);

        /*
            Not checking UI state as this will be checked during
            integration testing
         */

        //assert
        verify(hotel).addServiceCharge(roomNumber, serviceType, serviceCost);
        verify(recordServiceUI).displayServiceChargeMessage(eq(roomNumber), eq(serviceCost), any(String.class));
        assertTrue(ctl.isStateCompleted(), "Controller is not in COMPLETED state");
    }
}