package hotel.credit;

public class CreditCardHelper  {
	
	public CreditCard makeCreditCard(CreditCardType type, int number, int ccv) {
		CreditCard creditCard = new CreditCard(type, number, ccv);
		return creditCard;
	}

}
