package hotel.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hotel.credit.CreditCard;
import hotel.utils.IOUtils;

public class Room {
	
	public enum State {READY, OCCUPIED}
	
	int id;
	RoomType roomType;
	List<Booking> bookings;
	State state;

	
	public Room(int id, RoomType roomType) {
		this.id = id;
		this.roomType = roomType;
		bookings = new ArrayList<>();
		state = State.READY;
	}
	

	public String toString() {
		return String.format("Room : %d, %s", id, roomType);
	}


	public int getId() {
		return id;
	}
	
	public String getDescription() {
		return roomType.getDescription();
	}
	
	
	public RoomType getType() {
		return roomType;
	}
	
	public boolean isAvailable(Date arrivalDate, int stayLength) {
		IOUtils.trace("Room: isAvailable");
		for (Booking b : bookings) {
			if (b.doTimesConflict(arrivalDate, stayLength)) {
				return false;
			}
		}
		return true;
	}
	
	
	public boolean isReady() {
		return state == State.READY;
	}

        
        public boolean isOccupied() {
		return state == State.OCCUPIED;
	}


	public Booking book(Guest guest, Date arrivalDate, int stayLength, int numberOfOccupants, CreditCard creditCard) {
            Booking bookroom;
            bookroom = new Booking(guest, this, arrivalDate,
                    stayLength, numberOfOccupants, creditCard);
            bookings.add(bookroom);
            return bookroom;
	}


	public void checkin() {
                if (state != State.READY){
                    throw new RuntimeException("Error - The room is not in the correct state... 'READY'");
                }
                state = State.OCCUPIED;     //changes room state
	}


	public void checkout(Booking booking) {
            if (state != State.OCCUPIED){
                throw new  RuntimeException("Error - The room is not in the correct state... 'OCCUPIED'");
            }

            state = State.READY;
            bookings.remove(booking);       //changes room state
	}
}

