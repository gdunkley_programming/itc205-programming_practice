package hotel.service;

import hotel.entities.Booking;
import hotel.entities.Hotel;
import hotel.entities.ServiceType;
import hotel.utils.IOUtils;

import javax.swing.plaf.synth.SynthOptionPaneUI;

public class RecordServiceCTL {
	
	private static enum State {ROOM, SERVICE, CHARGE, CANCELLED, COMPLETED};
	
	private Hotel hotel;
	private RecordServiceUI recordServiceUI;
	private State state;
	
	private Booking booking;
	private int roomNumber;


	public RecordServiceCTL(Hotel hotel) {
		this.recordServiceUI = new RecordServiceUI(this);
		state = State.ROOM;
		this.hotel = hotel;
	}

	
	public void run() {		
		IOUtils.trace("PayForServiceCTL: run");
		recordServiceUI.run();
	}


	public boolean isStateRoom() {
		return state == State.ROOM;
	}


	public boolean isStateService() {
		return state == State.SERVICE;
	}


	public boolean isStateCharge() {
		return state == State.CHARGE;
	}


	public boolean isStateCancelled() {
		return state == State.CANCELLED;
	}


	public boolean isStateCompleted() {
		return state == State.COMPLETED;
	}


	public void roomNumberEntered(int roomNumber) {
		if (state != State.ROOM) {
			String mesg = String.format("PayForServiceCTL: roomNumberEntered : bad state : %s", state);
			throw new RuntimeException(mesg);
		}
		booking = hotel.findActiveBookingByRoomId(roomNumber);
		if (booking == null) {
			String mesg = String.format("No active booking for room id: %d", roomNumber);
			recordServiceUI.displayMessage(mesg);
		}
		else {
			this.roomNumber = roomNumber;
			state = State.SERVICE;
			recordServiceUI.setState(RecordServiceUI.State.SERVICE);
		}
	}
	
	
	public void serviceDetailsEntered(ServiceType serviceType, double cost) {
		if(state != State.SERVICE)
			throw new RuntimeException("Must be in state SERVICED");
		hotel.addServiceCharge(roomNumber, serviceType, cost);
		recordServiceUI.displayServiceChargeMessage(roomNumber, cost, serviceType.getDescription());
		state = State.COMPLETED;
		recordServiceUI.setState(RecordServiceUI.State.COMPLETED);
	}


	public void cancel() {
		recordServiceUI.displayMessage("Pay for service cancelled");
		state = State.CANCELLED;
		recordServiceUI.setState(RecordServiceUI.State.CANCELLED);
	}


	public void completed() {
		recordServiceUI.displayMessage("Pay for service completed");
	}


	

}
