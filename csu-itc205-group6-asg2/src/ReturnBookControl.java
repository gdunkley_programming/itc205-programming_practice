//ready for static review

public class ReturnBookControl {

    private ReturnBookUi ui;
    private enum ControlState { INITIALISED, READY, INSPECTING }
    private ControlState state;
    private Library library;
    private Loan currentLoan;


    public ReturnBookControl() {
        this.library = library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUi(ReturnBookUi ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("ReturnBookControl: cannot call setUi except in INITIALISED state");
        }
        this.ui = ui;
        ui.setState(ReturnBookUi.UiState.READY);
        state = ControlState.READY;
    }


    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
        }
        Book currentBook = library.getBook(bookId);

        if (currentBook == null) {
            ui.displayObject("Invalid book Id");
            return;
        }
        if (!currentBook.isOnLoan()) {
            ui.displayObject("book has not been borrowed");
            return;
        }
        currentLoan = library.getLoanByBookId(bookId);
        double overDueFine = 0.0;
        if (currentLoan.isOverDue()) {
            overDueFine = library.calculateOverDueFine(currentLoan);
        }
        ui.displayObject("Inspecting");
        ui.displayObject(currentBook.toString());
        ui.displayObject(currentLoan.toString());

        if (currentLoan.isOverDue()) {
            String fineMessage = String.format("\nOverdue fine : $%.2f", overDueFine);
            ui.displayObject(fineMessage);
        }
        ui.setState(ReturnBookUi.UiState.INSPECTING);
        state = ControlState.INSPECTING;
    }


    public void scanningComplete() {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
        }
        ui.setState(ReturnBookUi.UiState.COMPLETED);
    }


    public void dischargeLoan(boolean isDamaged) {
        if (!state.equals(ControlState.INSPECTING)) {
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
        }
        library.dischargeLoan(currentLoan, isDamaged);
        currentLoan = null;
        ui.setState(ReturnBookUi.UiState.READY);
        state = ControlState.READY;
    }
}