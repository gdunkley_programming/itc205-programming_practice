import java.util.Scanner;


public class BorrowBookUi {

    public static enum uiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
    private BorrowBookControl control;
    private Scanner input;
    private uiState state;


    public BorrowBookUi(BorrowBookControl control) {
        this.control = control;
        input = new Scanner(System.in);
        state = uiState.INITIALISED;
        control.setUi(this);
    }


    private String input(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }


    private void output(Object object) {
            System.out.println(object);
    }


    public void setState(uiState state) {
            this.state = state;
    }


    public void run() {
        output("Borrow book Use Case UI\n");
        while (true) {
            switch (state) {
                case CANCELLED:
                    output("Borrowing Cancelled");
                    return;

                case READY:
                    String scannedMemberId = input("Swipe member card (press <enter> to cancel): ");
                    if (scannedMemberId.length() == 0) {
                        control.cancel();
                        break;
                    }
                    try {
                        int memberId = Integer.valueOf(scannedMemberId).intValue();
                        control.swiped(memberId);
                    } catch (NumberFormatException e) {
                        output("Invalid member Id");
                    }
                    break;

                case RESTRICTED:
                    input("Press <any key> to cancel");
                    control.cancel();
                    break;

                case SCANNING:
                    String scannedBookId = input("Scan book (<enter> completes): ");
                    if (scannedBookId.length() == 0) {
                        control.complete();
                        break;
                    }
                    try {
                        int bookId = Integer.valueOf(scannedBookId).intValue();
                        control.scanned(bookId);
                    } catch (NumberFormatException e) {
                        output("Invalid book Id");
                    }
                    break;

                case FINALISING:
                    String answer = input("Commit loans? (Y/N): ");
                    if (answer.toUpperCase().equals("N")) {
                        control.cancel();
                    } else {
                        control.commitLoans();
                        input("Press <any key> to complete ");
                    }
                    break;

                case COMPLETED:
                    output("Borrowing Completed");
                    return;

                default:
                    output("Unhandled state");
                    throw new RuntimeException("BorrowBookUi : unhandled state :" + state);
            }
        }
    }


    public void display(Object object) {
        output(object);
    }
}