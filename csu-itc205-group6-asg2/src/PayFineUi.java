/*
Ready for review.
 */

import java.util.Scanner;


public class PayFineUi {

    public enum UiState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
    private PayFineControl payFineControl;
    private Scanner input;
    private UiState uiState;


    public PayFineUi(PayFineControl control) {
        this.payFineControl = control;
        input = new Scanner(System.in);
        uiState = UiState.INITIALISED;
        control.setUi(this);
    }


    public void setUiState(UiState uiState) {
        this.uiState = uiState;
    }


    public void run() {
        printOutput("Pay Fine Use Case UI\n");

        while (true) {
            switch (uiState) {
                case READY:
                    String scannedMemberId = readInput("Swipe member card (press <enter> to cancel): ");
                    if (scannedMemberId.length() == 0) {
                        payFineControl.cancel();
                        break;
                    }
                    try {
                        int memberId = Integer.valueOf(scannedMemberId).intValue();
                        payFineControl.cardSwiped(memberId);
                    } catch (NumberFormatException e) {
                        printOutput("Invalid memberId");
                    }
                    break;

                case PAYING:
                    double amount = 0;
                    String enteredAmount = readInput("Enter amount (<Enter> cancels) : ");
                    if (enteredAmount.length() == 0) {
                        payFineControl.cancel();
                        break;
                    }
                    try {
                        amount = Double.valueOf(enteredAmount).doubleValue();
                    } catch (NumberFormatException e) {}
                    if (amount <= 0) {
                        printOutput("Amount must be positive");
                        break;
                    }
                    payFineControl.payFine(amount);
                    break;

                case CANCELLED:
                    printOutput("Pay Fine process cancelled");
                    return;

                case COMPLETED:
                    printOutput("Pay Fine process complete");
                    return;

                default:
                    printOutput("Unhandled uiState");
                    throw new RuntimeException("FixBookUi : unhandled uiState :" + uiState);
            }
        }
    }


    private String readInput(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }


    private void printOutput(Object object) {
        System.out.println(object);
    }


    public void displayObject(Object object) {
        printOutput(object);
    }
}