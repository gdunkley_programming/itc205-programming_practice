import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collection;


@SuppressWarnings("serial")
public class Library implements Serializable {
    
    private static final String libraryFile = "Library.obj";
    private static final int loanLimit = 2;
    private static final int loanPeriod = 2;
    private static final double finePerDay = 1.0;
    private static final double maxFinesOwed = 5.0;
    private static final double damageFee = 2.0;

    private static Library self;
    private int bookId;
    private int memberId;
    private int libraryId;
    private Date loadDate;

    private Map<Integer, Book> catalog;
    private Map<Integer, Member> members;
    private Map<Integer, Loan> loans;
    private Map<Integer, Loan> currentLoans;
    private Map<Integer, Book> damagedBooks;


    private Library() {
        catalog = new HashMap<>();
        members = new HashMap<>();
        loans = new HashMap<>();
        currentLoans = new HashMap<>();
        damagedBooks = new HashMap<>();
        bookId = 1;
        memberId = 1;		
        libraryId = 1;		
    }


    public static synchronized Library getInstance() {
        if (self == null) {
            Path path = Paths.get(libraryFile);			
            if (Files.exists(path)) {	
                try (ObjectInputStream lineOfFile = new ObjectInputStream(new FileInputStream(libraryFile))) {
                    self = (Library) lineOfFile.readObject();
                    Calendar.getInstance().setDate(self.loadDate);
                    lineOfFile.close();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                self = new Library();
            }
        }
        return self;
    }


    public static synchronized void save() {
        if (self != null) {
            self.loadDate = Calendar.getInstance().getDate();
            try (ObjectOutputStream lineOfFile = new ObjectOutputStream(new FileOutputStream(libraryFile))) {
                lineOfFile.writeObject(self);
                lineOfFile.flush();
                lineOfFile.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    public int getBookId() {
        return bookId;
    }


    public int getMemberId() {
        return memberId;
    }


    private int nextBookId() {
        return bookId++;
    }


    private int nextMemberId() {
        return memberId++;
    }


    private int nextLibraryId() {
        return libraryId++;
    }


    public List<Member> getMembers() {
        Collection<Member> memberValues = members.values();
        return new ArrayList<Member>(memberValues);
    }


    public List<Book> getBooks() {
        Collection<Book> bookValues = catalog.values();
        return new ArrayList<Book>(bookValues);
    }


    public List<Loan> getCurrentLoans() {
        Collection<Loan> loanValues = currentLoans.values();
        return new ArrayList<Loan>(loanValues);
    }


    public Member addMember(String lastName, String firstName, String email, int phoneNo) {
        int nextMemberId = nextMemberId();
        Member member = new Member(lastName, firstName, email, phoneNo, nextMemberId);
        int memId = member.getId();
        members.put(memId, member);		
        return member;
    }


    public Book addBook(String author, String title, String callId) {
        int nextBookId = nextBookId();
        Book book = new Book(author, title, callId, nextBookId);
        int bookNum = book.getId();
        catalog.put(bookNum, book);		
        return book;
    }


    public Member getMember(int memberId) {
        if (members.containsKey(memberId)) {
            return members.get(memberId);
        }   
        return null;
    }


    public Book getBook(int bookId) {
        if (catalog.containsKey(bookId)) {
            return catalog.get(bookId);
        }
        return null;
    }


    public int getLoanLimit() {
        return loanLimit;
    }


    public boolean memberCanBorrow(Member member) {        
        if (member.getNumberOfCurrentLoans() == loanLimit) {
            return false;
        }
        if (member.getFinesOwed() >= maxFinesOwed) {
            return false;
        }
        for (Loan loan : member.getLoans()){
            if (loan.isOverDue()){
                return false;
            }
        }   
        return true;
    }


    public int loansRemainingForMember(Member member) {
        return loanLimit - member.getNumberOfCurrentLoans();
    }


    public Loan issueLoan(Book book, Member member) {
        Date dueDate = Calendar.getInstance().getDueDate(loanPeriod);
        Loan loan = new Loan(nextLibraryId(), book, member, dueDate);
        member.takeOutLoan(loan);
        book.borrowBook();
        int loanNum = loan.getId();
        loans.put(loanNum, loan);
        int bookNum = book.getId();
        currentLoans.put(bookNum, loan);
        return loan;
    }


    public Loan getLoanByBookId(int bookId) {
        if (currentLoans.containsKey(bookId)) {
            return currentLoans.get(bookId);
        }
        return null;
    }


    public double calculateOverDueFine(Loan loan) {
        if (loan.isOverDue()) {
            Date loanDueDate = loan.getDueDate();
            long daysOverDue = Calendar.getInstance().getDaysDifference(loanDueDate);
            double fine = daysOverDue * finePerDay;
            return fine;
        }
        return 0.0;
    }


    public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
        Member member = currentLoan.member();
        Book book  = currentLoan.book();

        double overDueFine = calculateOverDueFine(currentLoan);
        member.addFine(overDueFine);	

        member.dischargeLoan(currentLoan);
        book.returnBook(isDamaged);
        int bookNum = book.getId();
        if (isDamaged) {
            member.addFine(damageFee);
            damagedBooks.put(bookNum, book);
        }
        currentLoan.setLoanStateDischarged();
        currentLoans.remove(bookNum);
    }


    public void checkCurrentLoans() {
        for (Loan loan : currentLoans.values()) {
            loan.checkOverDue();
        }		
    }


    public void repairBook(Book currentBook) {
        int currBookNum = currentBook.getId();
        if (damagedBooks.containsKey(currBookNum)) {
            currentBook.repairBook();
            damagedBooks.remove(currBookNum);
        } else {
            throw new RuntimeException("Library: repairBook: book is not damaged");
        }
    }
}