/*
Ready for review.
 */

import java.util.Scanner;


public class FixBookUi {

    public enum UiState { INITIALISED, READY, FIXING, COMPLETED };
    private FixBookControl fixBookControl;
    private Scanner input;
    private UiState uiState;


    public FixBookUi(FixBookControl control) {
        this.fixBookControl = control;
        input = new Scanner(System.in);
        uiState = UiState.INITIALISED;
        control.setUi(this);
    }


    public void setUiState(UiState uiState) {
        this.uiState = uiState;
    }


    public void run() {
        displayOutput("Fix book Use Case UI\n");

        while (true) {
            switch (uiState) {
                case READY:
                    String bookStr = readInput("Scan book (<enter> completes): ");
                    if (bookStr.length() == 0) {
                        fixBookControl.scanningComplete();
                    } else {
                        try {
                            int bookId = Integer.valueOf(bookStr).intValue();
                            fixBookControl.bookScanned(bookId);
                        } catch (NumberFormatException e) {
                            displayOutput("Invalid bookId");
                        }
                    }
                    break;

                case FIXING:
                    String ans = readInput("Fix book? (Y/N) : ");
                    boolean fix = false;
                    if (ans.toUpperCase().equals("Y")) {
                        fix = true;
                    }
                    fixBookControl.fixBook(fix);
                    break;

                case COMPLETED:
                    displayOutput("Fixing process complete");
                    return;

                default:
                    displayOutput("Unhandled uiState");
                    throw new RuntimeException("FixBookUi : unhandled uiState :" + uiState);
            }
        }
    }


    private String readInput(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }


    private void displayOutput(Object object) {
        System.out.println(object);
    }


    public void displayObject(Object object) {
        displayOutput(object);
    }
}