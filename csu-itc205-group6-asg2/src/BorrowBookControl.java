import java.util.ArrayList;
import java.util.List;


public class BorrowBookControl {
    
    private BorrowBookUi ui;
    private Library library;
    private Member member;
    private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
    private ControlState state;
    private List<Book> pendingList;
    private List<Loan> completedList;
    private Book book;
	

    public BorrowBookControl() {
        this.library = library.getInstance();
        state = ControlState.INITIALISED;
    }


    public void setUi(BorrowBookUi ui) {
        if (!state.equals(ControlState.INITIALISED)){
            throw new RuntimeException("BorrowBookControl: cannot call setUi except in INITIALISED state");
        }
        this.ui = ui;
        ui.setState(BorrowBookUi.uiState.READY);
        state = ControlState.READY;
    }


    public void swiped(int memberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
        }
        member = library.getMember(memberId);
        if (member == null) {
            ui.display("Invalid memberId");
            return;
        }
        if (library.memberCanBorrow(member)) {
            pendingList = new ArrayList<>();
            ui.setState(BorrowBookUi.uiState.SCANNING);
            state = ControlState.SCANNING;
        } else {
            ui.display("member cannot borrow at this time");
            ui.setState(BorrowBookUi.uiState.RESTRICTED);
        }
    }


    public void scanned(int bookId) {
        book = null;
        if (!state.equals(ControlState.SCANNING)) {
            throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
        }	
        book = library.getBook(bookId);
        if (book == null) {
            ui.display("Invalid bookId");
            return;
        }
        if (!book.isAvailable()) {
            ui.display("book cannot be borrowed");
            return;
        }
        pendingList.add(book);
        for (Book bookVal : pendingList) {
            String bookDescription = bookVal.toString();
            ui.display(bookDescription);
        }
        int loansRemaining = library.loansRemainingForMember(member) - pendingList.size();
        if (loansRemaining == 0) {
            ui.display("Loan limit reached");
            complete();
        }
    }


    public void complete() {
        if (pendingList.size() == 0) {
            cancel();
        } else {
            ui.display("\nFinal Borrowing List");
            for (Book bookVal : pendingList) {
                String bookDescription = bookVal.toString();
                ui.display(bookDescription);
            }
            completedList = new ArrayList<Loan>();
            ui.setState(BorrowBookUi.uiState.FINALISING);
            state = ControlState.FINALISING;
        }
    }


    public void commitLoans() {
        if (!state.equals(ControlState.FINALISING)) {
            throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
        }
        for (Book bookVal : pendingList) {
            Loan loan = library.issueLoan(bookVal, member);
            completedList.add(loan);
        }
        ui.display("Completed Loan Slip");
        for (Loan loan : completedList) {
            String loanDescription = loan.toString();
            ui.display(loanDescription);
        }
        ui.setState(BorrowBookUi.uiState.COMPLETED);
        state = ControlState.COMPLETED;
    }


    public void cancel() {
        ui.setState(BorrowBookUi.uiState.CANCELLED);
        state = ControlState.CANCELLED;
    }
}