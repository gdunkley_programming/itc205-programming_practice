import java.io.Serializable;

//ready for static review

@SuppressWarnings("serial")
public class Book implements Serializable {

    private String title;
    private String author;
    private String callNo;
    private int id;
    private enum State { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
    private State state;


    public Book(String author, String title, String callNo, int id) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.id = id;
        this.state = State.AVAILABLE;
    }


    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("book: ").append(id).append("\n")
          .append("  Title:  ").append(title).append("\n")
          .append("  Author: ").append(author).append("\n")
          .append("  CallNo: ").append(callNo).append("\n")
          .append("  State:  ").append(state);

        return stringBuilder.toString();
    }


    public Integer getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }


    public boolean isAvailable() {
        return state == State.AVAILABLE;
    }


    public boolean isOnLoan() {
        return state == State.ON_LOAN;
    }


    public boolean isDamaged() {
        return state == State.DAMAGED;
    }


    public void borrowBook() {
        if (state.equals(State.AVAILABLE)) {
            state = State.ON_LOAN;
        } else {
            String exceptionMessage = String.format("book: cannot borrow while book is in state: %s", state);
            throw new RuntimeException(exceptionMessage);
        }
    }


    public void returnBook(boolean DAMAGED) {
        if (state.equals(State.ON_LOAN)) {
            if (DAMAGED) {
                state = State.DAMAGED;
            } else {
                state = State.AVAILABLE;
            }
        } else {
            String exceptionMessage = String.format("book: cannot Return while book is in state: %s", state);
            throw new RuntimeException(exceptionMessage);
        }
    }


    public void repairBook() {
        if (state.equals(State.DAMAGED)) {
            state = State.AVAILABLE;
        } else {
            String exceptionMessage = String.format("book: cannot repair while book is in state: %s", state);
            throw new RuntimeException(exceptionMessage);
        }
    }
}