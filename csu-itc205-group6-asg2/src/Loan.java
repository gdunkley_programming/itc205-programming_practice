/*
Ready for review.
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings("serial")
public class Loan implements Serializable {

    public enum LoanState { CURRENT, OVER_DUE, DISCHARGED };
    private int loanId;
    private Book book;
    private Member member;
    private Date dueDate;
    private LoanState loanState;
    private String memberFirstName;
    private String memberLastName;
    private int memberId;
    private int bookId;
    private String bookTitle;


    public Loan(int loanId, Book book, Member member, Date dueDate) {
        this.loanId = loanId;
        this.book = book;
        this.member = member;
        this.dueDate = dueDate;
        this.loanState = LoanState.CURRENT;
        this.memberFirstName = member.getFirstName();
        this.memberLastName = member.getLastName();
        this.memberId = member.getId();
        this.bookId = book.getId();
        this.bookTitle = book.getTitle();
    }


    public void checkOverDue() {
        if (loanState == LoanState.CURRENT &&
            Calendar.getInstance().getDate().after(dueDate)) {
            this.loanState = LoanState.OVER_DUE;
        }
    }


    public boolean isOverDue() {
        return loanState == LoanState.OVER_DUE;
    }


    public Integer getId() {
        return loanId;
    }


    public Date getDueDate() {
        return dueDate;
    }


    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDueDate = simpleDateFormat.format(dueDate);

        StringBuilder sb = new StringBuilder();
        sb.append("Loan:  ").append(loanId).append("\n")
          .append("  Borrower ").append(memberId).append(" : ")
          .append(memberLastName).append(", ").append(memberFirstName).append("\n")
          .append("  book ").append(bookId).append(" : " )
          .append(bookTitle).append("\n")
          .append("  DueDate: ").append(formattedDueDate).append("\n")
          .append("  State: ").append(loanState);
        return sb.toString();
    }


    public Member member() {
        return member;
    }


    public Book book() {
        return book;
    }


    public void setLoanStateDischarged() {
        loanState = LoanState.DISCHARGED;
    }
}