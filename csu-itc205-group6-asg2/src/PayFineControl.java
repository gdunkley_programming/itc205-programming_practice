/*
Ready for review.
 */

public class PayFineControl {

    private PayFineUi ui;
    private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
    private ControlState controlState;
    private Library library;
    private Member member;


    public PayFineControl() {
        this.library = library.getInstance();
        controlState = ControlState.INITIALISED;
    }


    public void setUi(PayFineUi ui) {
        if (!controlState.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("PayFineControl: cannot call setUi except in INITIALISED controlState");
        }
        this.ui = ui;
        ui.setUiState(PayFineUi.UiState.READY);
        controlState = ControlState.READY;
    }


    public void cardSwiped(int memberId) {
        if (!controlState.equals(ControlState.READY)) {
            throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY controlState");
        }
        member = library.getMember(memberId);

        if (member == null) {
            ui.displayObject("Invalid member Id");
            return;
        }
        String memberDescription = member.toString();
        ui.displayObject(memberDescription);
        ui.setUiState(PayFineUi.UiState.PAYING);
        controlState = ControlState.PAYING;
    }


    public void cancel() {
        ui.setUiState(PayFineUi.UiState.CANCELLED);
        controlState = ControlState.CANCELLED;
    }


    public double payFine(double amount) {
        if (!controlState.equals(ControlState.PAYING)) {
            throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING controlState");
        }
        double change = member.payFine(amount);
        if (change > 0) {
            String changeMessage = String.format("Change: $%.2f", change);
            ui.displayObject(changeMessage);
        }
        String memberDescription = member.toString();
        ui.displayObject(memberDescription);
        ui.setUiState(PayFineUi.UiState.COMPLETED);
        controlState = ControlState.COMPLETED;
        return change;
    }
}