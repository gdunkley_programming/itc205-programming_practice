/*
Ready for review.
 */

public class FixBookControl {

    private FixBookUi ui;
    private enum ControlState { INITIALISED, READY, FIXING };
    private ControlState controlState;
    private Library library;
    private Book currentBook;


    public FixBookControl() {
        this.library = library.getInstance();
        controlState = ControlState.INITIALISED;
    }


    public void setUi(FixBookUi ui) {
        if (!controlState.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("FixBookControl: cannot call setUi except in INITIALISED controlState");
        }
        this.ui = ui;
        ui.setUiState(FixBookUi.UiState.READY);
        controlState = ControlState.READY;
    }


    public void bookScanned(int bookId) {
        if (!controlState.equals(ControlState.READY)) {
            throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY controlState");
        }
        currentBook = library.getBook(bookId);
        if (currentBook == null) {
            ui.displayObject("Invalid bookId");
            return;
        }
        if (!currentBook.isDamaged()) {
            ui.displayObject("\"book has not been damaged");
            return;
        }
        String bookDescription = currentBook.toString();
        ui.displayObject(bookDescription);
        ui.setUiState(FixBookUi.UiState.FIXING);
        controlState = ControlState.FIXING;
    }


    public void fixBook(boolean fix) {
        if (!controlState.equals(ControlState.FIXING)) {
            throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING controlState");
        }
        if (fix) {
            library.repairBook(currentBook);
        }
        currentBook = null;
        ui.setUiState(FixBookUi.UiState.READY);
        controlState = ControlState.READY;
    }


    public void scanningComplete() {
        if (!controlState.equals(ControlState.READY)) {
            throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY controlState");
        }
        ui.setUiState(FixBookUi.UiState.COMPLETED);
    }
}