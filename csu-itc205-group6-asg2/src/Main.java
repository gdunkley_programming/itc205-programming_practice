import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//ready for static review

public class Main {

    private static Scanner inputScanner;
    private static Library library;
    private static String menu;
    private static Calendar calendar;
    private static SimpleDateFormat dateFormat;


    private static String getMenu() {
        StringBuilder builder = new StringBuilder();

        builder.append("\nLibrary Main Menu\n\n")
                .append("  M  : add member\n")
                .append("  LM : list members\n")
                .append("\n")
                .append("  B  : add book\n")
                .append("  LB : list books\n")
                .append("  FB : fix books\n")
                .append("\n")
                .append("  L  : take out a loan\n")
                .append("  R  : return a loan\n")
                .append("  LL : list loans\n")
                .append("\n")
                .append("  P  : pay fine\n")
                .append("\n")
                .append("  T  : increment date\n")
                .append("  Q  : quit\n")
                .append("\n")
                .append("Choice : ");

        return builder.toString();
    }


    public static void main(String[] args) {
        try {
            inputScanner = new Scanner(System.in);
            library = Library.getInstance();
            calendar = Calendar.getInstance();
            dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            for (Member member : library.getMembers()) {
                printOutput(member);
            }
            printOutput(" ");

            for (Book book : library.getBooks()) {
                printOutput(book);
            }

            menu = getMenu();
            boolean stopped = false;

            while (!stopped) {
                Date date = calendar.getDate();
                String formattedDate = dateFormat.format(date);
                printOutput("\n" + formattedDate);
                String menuChoice = getInput(menu);

                switch (menuChoice.toUpperCase()) {

                case "M":
                    addMember();
                    break;

                case "LM":
                    listMembers();
                    break;

                case "B":
                    addBook();
                    break;

                case "LB":
                    listBooks();
                    break;

                case "FB":
                    fixBooks();
                    break;

                case "L":
                    borrowBook();
                    break;

                case "R":
                    returnBook();
                    break;

                case "LL":
                    listCurrentLoans();
                    break;

                case "P":
                    payFine();
                    break;

                case "T":
                    incrementDate();
                    break;

                case "Q":
                    stopped = true;
                    break;

                default:
                    printOutput("\nInvalid option\n");
                    break;
                }
                Library.save();
            }
        } catch (RuntimeException exception) {
            printOutput(exception);
        }
        printOutput("\nEnded\n");
    }


    private static void payFine() {
        new PayFineUi(new PayFineControl()).run();
    }


    private static void listCurrentLoans() {
        printOutput("");
        for (Loan loan : library.getCurrentLoans()) {
            printOutput(loan + "\n");
        }
    }


    private static void listBooks() {
        printOutput("");
        for (Book book : library.getBooks()) {
            printOutput(book + "\n");
        }
    }


    private static void listMembers() {
        printOutput("");
        for (Member member : library.getMembers()) {
            printOutput(member + "\n");
        }
    }


    private static void borrowBook() {
        new BorrowBookUi(new BorrowBookControl()).run();
    }


    private static void returnBook() {
        new ReturnBookUi(new ReturnBookControl()).run();
    }


    private static void fixBooks() {
        new FixBookUi(new FixBookControl()).run();
    }


    private static void incrementDate() {
        try {
            String input = getInput("Enter number of days: ");
            int daysToIncrement = Integer.valueOf(input).intValue();
            calendar.incrementDate(daysToIncrement);
            library.checkCurrentLoans();
            Date date = calendar.getDate();
            String formattedDate = dateFormat.format(date);
            printOutput(formattedDate);
        } catch (NumberFormatException exception) {
             printOutput("\nInvalid number of days\n");
        }
    }


    private static void addBook() {
        String author = getInput("Enter author: ");
        String title  = getInput("Enter title: ");
        String callNo = getInput("Enter call number: ");
        Book book = library.addBook(author, title, callNo);
        printOutput("\n" + book + "\n");
    }


    private static void addMember() {
        try {
            String lastName = getInput("Enter last name: ");
            String firstName  = getInput("Enter first name: ");
            String email = getInput("Enter email: ");
            String input = getInput("Enter phone number: ");
            int phoneNo = Integer.valueOf(input).intValue();
            Member member = library.addMember(lastName, firstName, email, phoneNo);
            printOutput("\n" + member + "\n");
        } catch (NumberFormatException exception) {
             printOutput("\nInvalid phone number\n");
        }
    }


    private static String getInput(String prompt) {
        System.out.print(prompt);
        return inputScanner.nextLine();
    }


    private static void printOutput(Object objectToPrint) {
        System.out.println(objectToPrint);
    }
}