import java.util.Scanner;

//ready for static review

public class ReturnBookUi {

    public enum UiState { INITIALISED, READY, INSPECTING, COMPLETED }

    private ReturnBookControl control;
    private Scanner inputScanner;
    private UiState state;


    public ReturnBookUi(ReturnBookControl control) {
        this.control = control;
        inputScanner = new Scanner(System.in);
        state = UiState.INITIALISED;
        control.setUi(this);
    }


    public void run() {
        printOutput("Return book Use Case UI\n");

        while (true) {
            switch (state) {
                case INITIALISED:
                    break;

                case READY:
                    String scannedBookId = readInput("Scan book (<enter> completes): ");
                    if (scannedBookId.length() == 0) {
                        control.scanningComplete();
                    } else {
                        try {
                            int bookId = Integer.valueOf(scannedBookId).intValue();
                            control.bookScanned(bookId);
                        } catch (NumberFormatException e) {
                            printOutput("Invalid bookId");
                        }
                    }
                    break;

                case INSPECTING:
                    String answer = readInput("Is book damaged? (Y/N): ");
                    boolean isDamaged = false;
                    if (answer.toUpperCase().equals("Y")) {
                        isDamaged = true;
                    }
                    control.dischargeLoan(isDamaged);

                case COMPLETED:
                    printOutput("Return processing complete");
                    return;

                default:
                    printOutput("Unhandled state");
                    throw new RuntimeException("ReturnBookUi : unhandled state :" + state);
            }
        }
    }


    private String readInput(String prompt) {
        System.out.print(prompt);
        return inputScanner.nextLine();
    }


    private void printOutput(Object object) {
        System.out.println(object);
    }


    public void displayObject(Object object) {
        printOutput(object);
    }


    public void setState(UiState state) {
        this.state = state;
    }
}