S-ITC205_201860_B_D (Professional Programming Practice)
Assessment Item 4
Debugging
Graham Dunkley
Student number: 11583797
Subject Coordinator: Dr James Tulip


Bug1
Replication

The UAT file is located in the bitbucket repo under the folder directory listed below, 
It’s also has been uploaded to EAST’s as a separate file.

Directory: /itc205_ass4/src/Ass4_debugging/
File: TestCase_Bug1.docx.


Simplification

The automated test file is located in the following location below, listed below that is the output of the test file using debugger.

Directory: /itc205_ass4/test/hotel/checkout/
File: CheckoutCTLTestBug1.java

Tracing

The tracing of the bug was captured in a log file listed below in addition is the screen shots of some of the steps in the tracing of the error.

Directory: /itc205_ass4/src/Ass4_debugging/
File: DebuggingStepsBug1.log

Resolution

The UAT FIX file is located in the bitbucket repo under the folder directory listed below, 
It’s also has been uploaded to EAST’s as a separate file.

Directory: /itc205_ass4/src/Ass4_debugging/
File: TestCase_Bug1_FIX.docx.



Bug2
Replication

The UAT file is located in the bitbucket repo under the folder directory listed below, 
It’s also has been uploaded to EAST’s as a separate file.

Directory: /itc205_ass4/src/Ass4_debugging/
File: TestCase_Bug2.docx.


Simplification

The automated test file is located in the following location below, listed below is the output of the test file using debugger.

Directory: /itc205_ass4/test/hotel/checkout/
File: CheckoutCTLTestBug2.java


Tracing

The tracing of the bug was captured in a log file listed below in addition is the screen shots of some of the steps in the tracing of the error.

Directory: /itc205_ass4/src/Ass4_debugging/
File: DebuggingStepsBug2.log


Resolution

The UAT FIX file is located in the bitbucket repo under the folder directory listed below, 
It’s also has been uploaded to EAST’s as a separate file.

Directory: /itc205_ass4/src/Ass4_debugging/
File: TestCase_Bug2_FIX.docx.



