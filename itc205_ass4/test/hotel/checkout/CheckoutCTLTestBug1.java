/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.checkout;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import hotel.entities.ServiceCharge;
import hotel.entities.ServiceType;
import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.*;

/**
 *
 * @author graham
 */
@RunWith(MockitoJUnitRunner.class) 
public class CheckoutCTLTestBug1 {
    
    @Mock CheckoutUI checkoutUI;
    Booking booking;

    public @Rule ExpectedException exception = ExpectedException.none();
    
    int roomID = 101;
    double mycost = 150.15;
    int phone = 987654;
    int stay = 1;
    long confNum;
    int occupantNum = 1;
    int ccNumber = 1;
    int ccv = 1;
    
    CheckoutCTL control;
    Date arrivalDate;
    CreditCard creditCard;
    CreditCardType ccType = CreditCardType.VISA;
    SimpleDateFormat format;
    Hotel hotel = new Hotel();
    ServiceType serviceType = ServiceType.RESTAURANT;
    
    
    @Before
    public void setUp() throws Exception {
        
        creditCard = new CreditCard(ccType, ccNumber, ccv);
        
        hotel.registerGuest("John", "happy St", phone);
        Room room = new Room(roomID, RoomType.SINGLE);    
        hotel.addRoom(RoomType.SINGLE, roomID);           
        Guest guest = hotel.findGuestByPhoneNumber(phone);
        format = new SimpleDateFormat("dd-MM-yyyy");
        arrivalDate = format.parse("01-01-2001");
        confNum = hotel.book(room, guest, arrivalDate, stay, occupantNum, creditCard);
        hotel.checkin(confNum);
        
        control = new CheckoutCTL(hotel);

        MockitoAnnotations.initMocks(this);

        
        control.state = CheckoutCTL.State.ROOM;
        hotel.addServiceCharge(roomID, serviceType, mycost);
        
        
                        
                
        
    }

    /**
     * Test of roomIdEntered method, of class CheckoutCTL.
     * All Service charges are reported as 0.00 when checking out;
     */
    @Test
    public void testAllServiceChargesReportedAs$000() {
        System.out.println("arrange \n **************************************************");
        System.out.println("testAllServiceChargesReportedAs$000");
        System.out.println("Value of 'mycost' variable set @ $" + mycost);
        System.out.println();
        
        //arrange
        
        //act
        System.out.println("act \n **************************************************");
        control.roomIdEntered(roomID);
        
        //assert
        System.out.println("assert \n **************************************************");
        System.out.println("Value of 'mycost' variable set for a Restaurant @ $" + mycost);

    }
        
}
